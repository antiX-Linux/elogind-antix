elogind (252.9-1~rc1.0antix1) unstable; urgency=medium

  * antiX builds allow for runit scripts

 -- anticapitalista <antix@operamail.com>  Wed, 06 Sep 2023 14:47:36 +0300

elogind (252.9-1~rc1) experimental; urgency=medium

  * New upstream version 252.9.
  * Drop patches applied upstream.
  * Refresh patches.
  * d/elogind.init: revert workaround for #619; fixed upstream.
  * d/control:
    - add python3-jinja2 build-dep.
    - remove obsolete lsb-base dependency.
    - change policykit-1 recommends to polkitd (See: Debian#1025560).
  * d/not-installed: update pam.d/elogind-user path.
  * d/elogind.install:
    - include new /etc/elogind/sleep.conf.
    - add private library, now in /lib/<triplet>/elogind.
  * d/rules:
    - configure with mode=release.
  * Override lintian missing-systemd-service-for-init.d-script elogind.

 -- Mark Hindley <mark@hindley.org.uk>  Tue, 23 May 2023 11:06:28 +0100

elogind (246.10-5) unstable; urgency=medium

  * libelogind-compat: Fix package name in Breaks: libelogind0.

 -- Mark Hindley <mark@hindley.org.uk>  Tue, 26 Apr 2022 14:43:29 +0100

elogind (246.10-4) unstable; urgency=medium

  * Make libelogind0 coinstallable with libsystemd0 and add new
    libelogind-compat package.
  * Add new dummy-logind package.
  * Revert "Let elogind conflict with systemd" to allow coninstallation
    with dummy-systemd-dev.

 -- Mark Hindley <mark@hindley.org.uk>  Sat, 09 Apr 2022 13:39:05 +0100

elogind (246.10-3) unstable; urgency=medium

  * Prevent restart if startx session is running (Closes: #619).

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 27 Oct 2021 10:13:08 +0100

elogind (246.10-2) unstable; urgency=medium

  * Re-enable runtime only polkit support (Closes: #563).

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 05 Mar 2021 13:31:13 +0000

elogind (246.10-1) unstable; urgency=medium

  * Merge new upstream version v246.10.
  * Add missing Closes for #507 to version 246.9-1.
  * Remove unnecessary build-depends.
  * d/gbp.conf: update upstream-tag format.

 -- Mark Hindley <mark@hindley.org.uk>  Thu, 04 Mar 2021 17:24:33 +0000

elogind (246.9.1-1) unstable; urgency=medium

  * Merge v246.9.1.

 -- Mark Hindley <mark@hindley.org.uk>  Tue, 22 Dec 2020 16:25:53 +0000

elogind (246.9-1) unstable; urgency=medium

  [ Mark Hindley ]
  * Merge upstream v246.9 (Closes: #507).
  * Refresh quilt patch.
  * d/control: update Vcs-* following gitea migration.

  [ Unit193 ]
  * d/control: Rules-Requires-Root: no.
  * d/control: change to debhelper-compat.
  * d/control: cleanup wrapping and whitespace.
  * d/control: update to Standards Version 4.5.1 (no changes).
  * d/control: remove dpkg-dev build-dep that is automatically satisfied.
  * d/copyright: use HTTPS scheme for url.

 -- Mark Hindley <mark@hindley.org.uk>  Sat, 19 Dec 2020 16:44:07 +0000

elogind (243.8-1) unstable; urgency=medium

  * New upstream version 243.8.
  * d/control: update Vcs-* fields following Gitea migration.
  * Change rootlibdir to /lib (closes: #482).
  * Document solutions for libelogind0/libsystemd-dev build-depends
    conflict. (closes: #435).

 -- Mark Hindley <mark@hindley.org.uk>  Sat, 20 Jun 2020 10:59:46 +0100

elogind (243.7-2) unstable; urgency=medium

  * Include 70-power-switch.rules (closes: #423).
  * Add breaks/replaces for Debian's udev which also ships the same file.

 -- Mark Hindley <mark@hindley.org.uk>  Tue, 28 Apr 2020 11:22:51 +0100

elogind (243.7-1) unstable; urgency=medium

  * New upstream version 243.7.
  * Explicitly enable manpages in build (disabled upstream).
  * Remove NEWS file from packaging (removed upstream).
  * Update to Standards version 4.5.0 (no changes).
  * Remove skip_id128_tests_unsupported.patch: included upstream.
  * Default to false for RemoveIPC (Closes: #949698).
  * d/control: add Origin: Devuan.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 16 Mar 2020 18:44:05 +0000

elogind (241.4-2) unstable; urgency=medium

   * Remove transitional libsystemd0 package.

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 01 Jan 2020 15:03:47 +0000

elogind (241.4-1) unstable; urgency=medium

   * Merge new upstream v241.4.
   * Move libsystemd ABI compatibility details to README.Debian.

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 27 Dec 2019 13:15:24 +0000

elogind (241.3-4) unstable; urgency=medium

  * Revert Increase build tests timeout to 90 to accommodate armel and armhf.
  * Try to detect broken khash support better. When crossbuilding, qemu doesn't
    support setsockopt() SOL_ALG. Skip tests if this fails.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 23 Dec 2019 17:22:22 +0000

elogind (241.3-3) unstable; urgency=medium

  * Increase build tests timeout to 90 to accommodate armel and armhf.

 -- Mark Hindley <mark@hindley.org.uk>  Thu, 05 Dec 2019 17:45:27 +0000

elogind (241.3-2) unstable; urgency=medium

  * New transitional package libsystemd0 which depends on libelogind0.
  * Add details of sd-daemon(3) implementation to libelogind0 package
    description.
  * elogind Recommends libpam-elogind.
  * Update to standards version 4.4.1 (no changes).

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 07 Oct 2019 22:55:44 +0100

elogind (241.3-1) unstable; urgency=medium

  [ Mark Hindley ]
  * New upstream release v241.3.
  * Upgrade to Standards Version 4.4.0:
    - libpam-elogind provides new virtual packages logind and default-logind.
  * Correctly license AppStream metadata as CC0-1.0 in d/copyright.

  [ Andreas Messer ]
  * Retire from maintenance of this package.

  [ Andreas Beckmann ]
  * Simplify libsystemd.so compatibility symlink setup. (Closes: #926591)

 -- Mark Hindley <mark@hindley.org.uk>  Sun, 14 Jul 2019 11:16:52 +0100

elogind (241.1-1) unstable; urgency=medium

  * New upstream version v241.1.
  * libelogind0 is ABI compatible with libsystemd0 so conflict, replace and
    provide libsystemd0 (=${source:Upstream-Version}), and also install
    libsystemd.so symlinks (Closes: #923244).
  * Change AppStream metadata license to LGPL-2.1+ to match other files in
    debian/.
  * Move libelogind-dev-doc to Section: doc.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 11 Mar 2019 18:08:28 +0000

elogind (239.3+20190131-1) unstable; urgency=medium

  * New snapshot of upstream/v239-stable.
  * Remove local patches now fixed upstream.
  * Use new SIGINT handler for graceful restarts in initscript.
  * Specify devuan branch in Vcs-* fields.
  * Update to Standards version 4.3.0 (no changes).
  * libelogind-dev-doc conflicts with libsystemd-dev as they share manpages.
  * Set AppStream component id to begin with a reverse domain name.
  * Add Breaks and Replaces libelogind-dev (<< 239.1+20181115-1) to
    libelogind-dev-doc (Closes: #916768).

 -- Mark Hindley <mark@hindley.org.uk>  Thu, 31 Jan 2019 18:57:50 +0000

elogind (239.3-3) unstable; urgency=medium

  * Preserve session and seat data when restarting (Closes: #916247).

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 14 Dec 2018 00:48:03 +0000

elogind (239.3-2) UNRELEASED; urgency=medium

  * Fix stale pidfile handling (Closes: #916212).
  * Add separate reload option to initscript.
  * Correct typo in Vcs-Git url.

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 13 Dec 2018 11:26:15 +0000

elogind (239.3-1) unstable; urgency=medium

  * New upstream release.
  * Delete Dbus_lib_machine-id patch, now included upstream.
  * Delete manpage-branding.diff, now incorporated upstream.
  * Use CC0-1.0 for AppStream metadata license.
  * Correct typos in debian/rules comments.

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 07 Dec 2018 10:31:31 +0000

elogind (239.2-1) unstable; urgency=medium

  * New upstream version 239.2.
  * Enable compile-time testsuite which depends on dbus.
  * Include busctl manpage.
  * Support /var/lib/dbus/machine-id as alternative to /etc/machine-id.
  * Enable SELinux support.

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 23 Nov 2018 11:04:04 +0000

elogind (239.1+20181115-1) unstable; urgency=medium

  * Upstream fix to address excessive delay closing sessions.
  * To build polkit, we will need to install libsystemd-dev and libelogind-dev
    simlultaneously. So
     - move dev manpages to separate package.
     - no longer conflict -dev package with libsystemd-dev.
  * Disable Dbus activation by default.
  * Remove upstream pwx git submodule. Thanks to Ian Jackson.

 -- Mark Hindley <mark@hindley.org.uk>  Thu, 15 Nov 2018 10:49:15 +0000

elogind (239.1-0.1) unstable; urgency=medium

  [ Mark Hindley ]
  * New upstream release v239.1.
  * Rework packaging for meson buildsystem.
  * Change libpam-elogind Breaks dependencies to Conflicts.
  * Update to debhelper compat 11.
  * Update to Standards version 4.2.1 (no changes).
  * Enable LTO build as it works with binutils from unstable (2.31.1).
  * Include manpages in -dev package.
  * Move documentation consistency changes to a quilt patch.
  * Add AppStream metainfo.
  * Add upstream watchfile.
  * Don't package /lib/udev/rules.d/70-power-switch.rules which is also in udev.
  * Configure with -Dsplit-usr=true to ensure consistent build.
  * Fix SIGABRT on daemonization caused by freeing overlapping pointers to
    program_invocation[_short]_name (Upstream:
    https://github.com/elogind/elogind/issues/92).
  * New version of /etc/pam.d/elogind-user based on Debian systemd.

  [ Andreas Messer ]
  * fix debian branch in gbp.conf
  * change upstream version tagname

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 07 Nov 2018 00:00:29 +0000

elogind (234.4-2~exp1) experimental; urgency=medium

  * debian/rules
    - Make libpam-elogind break libpam-ck-connector
  * enable pristine-tar

 -- Andreas Messer <andi@bastelmap.de>  Thu, 22 Feb 2018 19:49:52 +0100

elogind (234.4-1+devuan1.4) experimental; urgency=medium

  *  debian/libelogind-dev.install
    -  Drop manpages to allow installation in parallel
       with libsystemd-dev
  * debian/extras/elogind
    - enable elogind pam by default

 -- Andreas Messer <andi@bastelmap.de>  Wed, 07 Feb 2018 22:23:46 +0100

elogind (234.4-1+devuan1.3) experimental; urgency=medium

  * debian/patches/fix-forking.diff
    - Fix startpar not erminating due to open file descriptors after
      forking

 -- Andreas Messer <andi@bastelmap.de>  Sat, 27 Jan 2018 12:44:32 +0100

elogind (234.4-1+devuan1.2) experimental; urgency=medium

  * debian/rules
    - disable killing background processes on logout

 -- Andreas Messer <andi@bastelmap.de>  Sat, 20 Jan 2018 16:53:32 +0100

elogind (234.4-1+devuan1) experimental; urgency=medium

  * Initial release.

 -- Andreas Messer <andi@bastelmap.de>  Thu, 04 Jan 2018 19:26:09 +0100
